#!/bin/bash

EXE_SUFFIX=.x

for i in $(find ./testbench -type f -name "Makefile"); do
    path=$(dirname $i)
    make clean -C $path EXE_SUFFIX=$EXE_SUFFIX
done

rm -rf dump
