#!/bin/bash

set -e

CC=arm-linux-gnueabihf-gcc
CXX=arm-linux-gnueabihf-g++
DUMP=arm-linux-gnueabihf-objdump

EXE_SUFFIX=".x"

for i in $(find ./testbench -type f -name "Makefile"); do
    path=$(dirname $i)
    make -C $path CC=$CC CXX=$CXX EXE_SUFFIX=$EXE_SUFFIX
done

mkdir -p dump
for bin in $(find ./testbench -type f -name "*$EXE_SUFFIX"); do
    path=$(dirname $bin)
    bench=$(basename -s .x $bin)

    $DUMP -D $bin > dump/$bench.dump
done
